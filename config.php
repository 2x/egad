<?php

class Config {
    
    public static $DATABASE = array (
        // Data Source Name, contains the information required to connect to the database
        'dsn'      => 'mysql:host=localhost;dbname=freepost_freepost;charset=utf8',
        'username' => 'freepost',
        'password' => 'freepost'
    );
    
    public static $SEND_EMAILS = true;
}
